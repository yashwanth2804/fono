const initalState ={
   
    cart : []
}

const MarkedList = (state=initalState,action) =>{

    const newState = {...state};
    if(action.type === "BUY_NOW"){

         
        const index = action.phones.findIndex( note => note.id === action.pId );
      
        const updatedCart = action.phones[index];
     
         return {...state, cart:[...state.cart,updatedCart] };
       

    }else if (action.type === "REMOVE"){
        let updatedPhones = action.cart.filter( t => t.id !== action.pId);
        return {...state,cart:updatedPhones}
    }
   
    return newState;
}

export default MarkedList;