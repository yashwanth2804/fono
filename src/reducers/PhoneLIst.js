const initalState ={
    phones : []
   
}

const PhoneList = (state=initalState,action) =>{
    
    const newState = {...state};
    if(action.type === "REMOVE"){
        //add the id to phones list 
        const index = action.cart.findIndex( note => note.id === action.pId );
      
        const updatedCart = action.cart[index];
     
         return {...state, phones:[...state.phones,updatedCart] };
        
    }else if (action.type === "BUY_NOW"){
         
        
        //now delete on id from it 
        let updatedPhones = action.phones.filter( t => t.id !== action.pId);
        return {...state,phones:updatedPhones}
         
    } 
    if(action.type === "Load_PHONES"){
      
        return {...state,phones:action.phonesList}
    }
    return newState;
}

export default PhoneList;