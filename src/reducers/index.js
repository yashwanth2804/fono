import {combineReducers} from 'redux';
import PhoneList from './PhoneLIst';
import MarkedList from './MarkedLIst';

const rootReducer = combineReducers({
    phoneR:PhoneList,
    marked:MarkedList
});

export default rootReducer;

