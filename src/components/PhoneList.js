import React, { Component } from 'react'
import * as actionCreators from '../actions/index';
import { connect } from 'react-redux';
//import  phoneFetchAPI  from '../actions/index'
class phoneList extends Component {
 
    render() {
        console.log(this.props.phones);
        const phonesListmap = this.props.phones.map((p) => {
            return (

                <div key={p.id}>
                    {p.DeviceName}
                    <button onClick={() => this.props.byNow(p.id)}>click</button>
                </div>
            );
        });

        return (
            <div>
                {phonesListmap}
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return (
        {
            phones: state.phoneR.phones
        }
    )

}

const mapDispathToProps = (dispatch) => {
    
    return ({
       
         byNow: (pId) => { dispatch(actionCreators.buyNow(pId)) },
       

    });
}
export default connect(mapStateToProps, mapDispathToProps)(phoneList);