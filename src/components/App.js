import React, { Component } from 'react';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import { connect } from 'react-redux';

import * as actionCreators from '../actions/index';
import PhoneList from './PhoneList';
import PhoneList1 from './PhoneList1';

class App extends Component {
  componentDidMount() {
    const uri = "https://fonoapi.freshpixl.com/v1/getlatest?token=0e01fbdbf7785949e29a52ed8fff4051c64371b304119370&limit=20";
    // this.props.dispatch(actionCreators.phoneFetchAPI(uri));
    console.log("component dispatch")
    this.props.onLoad(uri);
  }

  render() {
    return (
      <div >
        <div className="container">
          <div className="row">
            <div className="col s6"><PhoneList /></div>
            <div className="col s6"><PhoneList1 /></div>
          </div>

        </div>
      </div>
    );
  }

}
const mapDispathToProps = (dispatch) => {
  return ({
    //  byNow: (pId) => { dispatch(actionCreators.buyNow(pId)) },
    onLoad: (uri) => { dispatch(actionCreators.phoneFetchAPI(uri)) }

  });
}
export default connect(null, mapDispathToProps)(App);
