import axios from 'axios';
import uuidv4 from 'uuid';


export const removeCart = (pId) => {
    //redux thunk
    return(dispatch,getState) =>{
        const phones = getState().phoneR.phones;
       const cart = getState().marked.cart;
       dispatch({type:"REMOVE",pId: pId,phones:phones,cart: cart});
    }
}


export const buyNow = (pId) => {
    return (dispatch,getState) => {
        const phones = getState().phoneR.phones;
       const cart = getState().marked.cart;
        dispatch({type: 'BUY_NOW', pId: pId,phones:phones,cart: cart });
        
    }
};

export const loadPhones = (phonesList) => (
    { type: 'Load_PHONES', phonesList: phonesList }
);

export const  phoneFetchAPI = (url) => {
    console.log(url);
    return (dispatch) => {

        axios
        .get(
            url
        )
        .then(
            f =>  {
                
                const g = f.data;
                g.forEach(element => {
                    element.id = uuidv4();

                });
                 return g;
            }
            
        )
        .then(res => dispatch(loadPhones(res)))
        .catch(error => console.log("errrrr"));;

      
    }
}; 
